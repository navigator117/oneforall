package com.navi.plugins;

import java.util.logging.Logger;

import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaWebView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.tencent.tauth.IUiListener;
import com.tencent.tauth.Tencent;
import com.tencent.tauth.UiError;


/**
 * This class echoes a string called from JavaScript.
 */
public class OneForAll extends CordovaPlugin {
	
	public static final String ACTION_SET_USER_INFO = "setUserInfo";
	public static final String ACTION_LOGIN = "login";
	public static final String ACTION_LOGOUT= "logout";
	
	public static final String LOGIN_INFO_OPENID = "openid";
	public static final String LOGIN_INFO_ACCESS_TOKEN = "access_token";
	public static final String LOGIN_INFO_EXPIRES_IN = "expires_in";
	
	public static final String LOGIN_ERROR_CANCELED = "canceled";
	public static final String LOGIN_ERROR_CODE = "code";
	public static final String LOGIN_ERROR_MESSAGE = "message";
	public static final String LOGIN_ERROR_DETAIL = "detail";
	
	static Logger logger = Logger.getLogger(OneForAll.class.getName());
	
	private Tencent tencent = null;
	
	private class LoginListener implements IUiListener {
		
		private Tencent tencent = null;
		private CallbackContext callbackContext = null;
    	
		public LoginListener(Tencent tencent, CallbackContext callbackContext) {
			this.tencent = tencent;
			this.callbackContext = callbackContext;
		}

		@Override
		public void onComplete(Object r) {
			JSONObject result = (JSONObject)r;
			JSONObject response = new JSONObject();
			try {
				String openId = (result.has(LOGIN_INFO_OPENID) ? result.getString(LOGIN_INFO_OPENID) : this.tencent.getOpenId());
				String accessToken = (result.has(LOGIN_INFO_ACCESS_TOKEN) ? result.getString(LOGIN_INFO_ACCESS_TOKEN) : this.tencent.getAccessToken());
				Long expiresIn = System.currentTimeMillis() + (result.has(LOGIN_INFO_EXPIRES_IN) ? result.getLong(LOGIN_INFO_EXPIRES_IN) : this.tencent.getExpiresIn()) * 1000;				
				response.put(LOGIN_INFO_OPENID, openId);
				response.put(LOGIN_INFO_ACCESS_TOKEN, accessToken);
				response.put(LOGIN_INFO_EXPIRES_IN, expiresIn);
			}
			catch (JSONException e) {
			}			
			this.callbackContext.success(response);
		}		
		
		@Override
		public void onCancel() {
			JSONObject response = new JSONObject();
			try {
				response.put(LOGIN_ERROR_CANCELED, true);				
			}
			catch (JSONException e) {
			}			
			this.callbackContext.error(response);
		}
		
		@Override
		public void onError(UiError error) {
			JSONObject response = new JSONObject();
			try {
				response.put(LOGIN_ERROR_CANCELED, false);
				response.put(LOGIN_ERROR_CODE, error.errorCode);
				response.put(LOGIN_ERROR_MESSAGE, error.errorMessage);
				response.put(LOGIN_ERROR_DETAIL, error.errorDetail);				
			}
			catch (JSONException e) {
			}
			this.callbackContext.error(response);
		}    	
	};
	
	private class LoginAction implements Runnable {
		
		private Tencent tencent = null;
		private CallbackContext callbackContext = null;
		
		public LoginAction(Tencent tencent, CallbackContext callbackContext) {
			this.tencent = tencent;
			this.callbackContext = callbackContext;
		}
		
		@Override
		public void run() { 	
	    	this.tencent.login(cordova.getActivity(), Settings.QQ_CONNECT_LOGIN_SCOPE, new LoginListener(this.tencent, this.callbackContext)); 				
		}		
	};
	
	@Override	
	public void initialize(CordovaInterface cordova, CordovaWebView webView) {
		super.initialize(cordova, webView);
		Settings.load(this.cordova.getActivity().getApplicationContext());
		this.tencent = Tencent.createInstance(Settings.QQ_CONNECT_APP_ID, this.cordova.getActivity().getApplicationContext());		
	}
	
    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
    	if (action.equals(ACTION_SET_USER_INFO) && args.length() == 1) {
    		this.setUserInfo(args.getJSONObject(0), callbackContext);
    		return true;
    	}
    	else if (action.equals(ACTION_LOGIN) && args.length() == 0) {
        	this.login(callbackContext);
        	return true;
        	
        }
    	else if (action.equals(ACTION_LOGOUT) && args.length() == 0) {
    		this.logout(callbackContext);    		
    		return true;
    	}
        return false;
    }
    
    private void setUserInfo(JSONObject userInfo, CallbackContext callbackContext) {
    	try {
    		this.tencent.setOpenId(userInfo.getString(LOGIN_INFO_OPENID));
    		this.tencent.setAccessToken(userInfo.getString(LOGIN_INFO_ACCESS_TOKEN), Long.toString((userInfo.getLong(LOGIN_INFO_EXPIRES_IN) - System.currentTimeMillis()) / 1000));
    		callbackContext.success();
    	}
    	catch (JSONException e) {    	
    		callbackContext.error("Invalid arguments.");
    	}    	
    }
    
    private void login(CallbackContext callbackContext) {
    	this.cordova.getThreadPool().execute(new LoginAction(this.tencent, callbackContext));
    }
    
    private void logout(CallbackContext callbackContext) {
    	this.tencent.logout(this.cordova.getActivity().getApplicationContext());
    	callbackContext.success();
    }    
}
