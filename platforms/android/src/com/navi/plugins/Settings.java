package com.navi.plugins;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;

public final class Settings {	
	
	public static final    String QQ_CONNECT_LOGIN_SCOPE = "get_user_info";
	public static volatile String QQ_CONNECT_APP_ID;
	public static volatile String QQ_CONNECT_APP_KEY;
	
	public static void load(Context context) {		
        ApplicationInfo ai = null;
        try {
            ai = context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA);
        } catch (PackageManager.NameNotFoundException e) {
            return;
        }
        if (ai == null || ai.metaData == null) {
            return;
        }        
        QQ_CONNECT_APP_ID  = ai.metaData.getString("QQ_CONNECT_APP_ID");
        QQ_CONNECT_APP_KEY = ai.metaData.getString("QQ_CONNECT_APP_KEY");
	}
}
