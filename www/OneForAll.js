var exec = require('cordova/exec');

exports.constants = {
	LOGIN_INFO_OPENID: "openid",
	LOGIN_INFO_ACCESS_TOKEN: "access_token",
	LOGIN_INFO_EXPIRES_IN: "expires_in",

    LOGIN_ERROR_CANCELED: "canceled",
	LOGIN_ERROR_CODE: "code",
	LOGIN_ERROR_MESSAGE: "message",
	LOGIN_ERROR_DETAIL: "detail"
};

exports.setUserInfo = function(userInfo, successCallback, errorCallback) {
    exec(successCallback, errorCallback, "OneForAll", "setUserInfo", [userInfo]);
};

exports.login = function(successCallback, errorCallback) {
    exec(successCallback, errorCallback, "OneForAll", "login", []);
};

exports.logout = function(successCallback, errorCallback) {
    exec(successCallback, errorCallback, "OneForAll", "logout", []);
};