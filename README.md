One For All
==============
   	
All-In-One plugin used in ItsOnMe Project.
   	
PLUGIN INSTALL
----------------

cordova plugin add com.navi.plugins.oneforall --variable QQ_CONNECT_APP_ID=XXXX --variable QQ_CONNECT_APP_KEY=XXXX

HELP
--------------

[Source code] (https://navigator117@bitbucket.org/navigator117/oneforall.git)